import React, { useState, useEffect, useCallback } from 'react';
import { connect } from 'react-redux'
import { commonApi, validationMessages,storeDetails, showSuccessToast } from '../../redux/actions'
import { Select, Spin } from 'antd';
import queryString from "query-string";
import ImageCrop from '../../components/imageCrop'


const AddSingleProduct = ({ storeDetails,...props }) => {
    const [formData, setFormData] = useState({
        step1: true,
        step2: false,
        step1Finished: false,
        selectedStore: '',
        selectedStoreId: '',
        selectedCategoryId: '',
        productName: '',
        description: '',
        brandName: '',
        selectedCategory: '',
        categoryList: [],
        storesList: [],
        box: "",
        sku: '',
        barcode: '',
        imagesList: [],
        prevImages: [],
        imgs_Arr: [],
        multipleUpload: false,
        productSlug: '',
        typically_availablity: '',
        availableDays: '',


        //step2
        weight_measure: '',
        total_weight: '',
        packaging_length: '',
        packaging_width: '',
        packaging_height: '',
        standard_cost: '',
        unit_cost: '',
        volume_discount: '',
        volume_unit_cost: '',
        availability: '',
        noOfpcs: '',
        tags: [],
        errors: {},
        prod_slug: '',
    })
    const [loading, setLoading] = useState(false)
    const authorized = true

    //@Purpose : get the details of product and autofill in fields if it is edit page
    useEffect(() => {
        let url = props.location.search;
        let params = queryString.parse(url);
        if (params && params.slug) {
            // setFormData((prevState) => ({ ...prevState, prod_slug: params.slug }));
            async function getProdDetails() {
                let response = await commonApi(`products/${params.slug}`, '', 'get', authorized)
                if (response && response.status === 200) {
                    let { data } = response.data
                    let tags = []
                    data.tags.length !== 0 && data.tags.map((tag) => {
                        return tags.push(tag.name)
                    })
                    setFormData({
                        ...formData,
                        selectedStore: data.store && data.store.store_name,
                        selectedStoreId: data.store && data.store.id,
                        productName: data.name,
                        description: data.description,
                        brandName: data.brand_name,
                        selectedCategory: data.category && data.category.name,
                        selectedCategoryId: data.category && data.category.id?data.category.id:'',
                        box: data.item_kit,
                        sku: data.sku,
                        barcode: data.barcode,
                        prevImages: data.images,

                        weight_measure: data.weight_measure?data.weight_measure:'Kg',
                        total_weight: data.total_weight,
                        packaging_length: data.packaging_length,
                        packaging_width: data.packaging_width,
                        packaging_height: data.packaging_height,
                        standard_cost: data.standard_cost,
                        unit_cost: data.unit_cost,
                        volume_unit_cost: data.volume_unit_cost,
                        volume_discount: data.volume_discount,
                        availability: data.availability,
                        noOfpcs: data.no_of_pcs,
                        typically_availablity: data.typically_availablity && data.typically_availablity === 'In Stock' ? data.typically_availablity : (data.typically_availablity !== null ? data.typically_availablity.split(" ")[1] : ''),
                        availableDays: data.typically_availablity && data.typically_availablity !== 'In Stock' ? data.typically_availablity && data.typically_availablity !== null && data.typically_availablity.split(" ")[0] : "",
                        tags: tags,
                        prod_slug: params.slug
                    })
                }
            }
            getProdDetails();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    //@Purpose : handle change for input fields and to remove error messages
    const handleChange = (e) => {
        if (e.target.value) {
            setFormData({
                ...formData,
                [e.target.name]: e.target.value,
                errors: Object.assign(formData.errors, { [e.target.name]: "" }),
            });
        } else
            setFormData({
                ...formData,
                [e.target.name]: "",
            });
    };


    //@Purpose : get categories list
    const CategoriesList = async () => {
        let response = await commonApi('categories', '', 'get', authorized)
        if (response && response.data) {
            let { data } = response
            setFormData({ ...formData, categoryList: data })
        }
    }


    //@Purpose : get store list
    const getStoreList = async () => {
        let response = await commonApi('stores-list', '', 'get', authorized)
        if (response && response.data) {
            let { data } = response
            setFormData({ ...formData, storesList: data })
        }
    }


    //@Purpose : validating step1 brfore submit
    const step1validateForm = () => {
        let { selectedStore, selectedCategory, productName, box, sku } = formData
        let errors = {},
            isValid = true;
        if (!productName || productName.trim() === "") {
            isValid = false;
            errors["productName"] = validationMessages.productName;
        }
        if (!sku || sku.trim() === "") {
            isValid = false;
            errors["sku"] = validationMessages.sku;
        }
        // if (!description || description.trim() === "") {
        //     isValid = false;
        //     errors["description"] = validationMessages.description;
        // }
        // if (!barcode || barcode.trim() === "") {
        //     isValid = false;
        //     errors["barcode"] = validationMessages.barcode;
        // }
        // if (!brandName || brandName.trim() === "") {
        //     isValid = false;
        //     errors["brandName"] = validationMessages.brandName;
        // }
        if (!selectedStore) {
            isValid = false;
            errors["selectedStore"] = validationMessages.selectedStore;
        }
        if (!selectedCategory) {
            isValid = false;
            errors["selectedCategory"] = validationMessages.selectedCategory;
        }
        if (!box || box.trim() === "") {
            isValid = false;
            errors["box"] = validationMessages.box;
        }
        setFormData({ ...formData, errors: errors });
        return isValid;
    };

    //@Purpose : validating step2 before submit
    const step2validateForm = () => {
        let { unit_cost, standard_cost, availability, availableDays, tags, typically_availablity } = formData
        let errors = {},
            isValid = true;
        // if (!weight_measure || weight_measure.trim() === "") {
        //     isValid = false;
        //     errors["weight_measure"] = validationMessages.weight_measure;
        // }
        // if (!volume_discount || volume_discount.trim() === "") {
        //     isValid = false;
        //     errors["volume_discount"] = validationMessages.volume_discount;
        // }
        if (!unit_cost || unit_cost.trim() === "") {
            isValid = false;
            errors["unit_cost"] = validationMessages.unit_cost;
        }
        if (!standard_cost || standard_cost.trim() === "") {
            isValid = false;
            errors["standard_cost"] = validationMessages.standard_cost;
        }
        if (!availability || availability.trim() === "") {
            isValid = false;
            errors["availability"] = validationMessages.availability;
        }
        if (typically_availablity && typically_availablity !== 'In Stock') {
            if (!availableDays || availableDays.trim() === "") {
                isValid = false;
                errors["availableDays"] = validationMessages.availableDays;
            }
        }
        if (tags.length === 0) {
            isValid = false;
            errors["tags"] = validationMessages.tags;
        }
        setFormData({ ...formData, errors: errors });
        return isValid;
    };



    //@Purpose : to select multiple images and preview them using base64 path
    const fileChangedHandler = (e) => {
        var imagesList = formData.imagesList;
        var files = e.target.files;
        var imgs_Arr = []
        var ins = files.length;
        for (var x = 0; x < ins; x++) {
            imgs_Arr.push(files[x])
        }
        setFormData((prevState) => ({ ...prevState, imgs_Arr: imgs_Arr }));
        for (var i = 0, f; f = files[i]; i++) {
            if (!f.type.match('image.*')) {
                continue;
            }
            var reader = new FileReader();
            reader.onload = (function () {
                return function (e) {
                    imagesList.push({ image: e.target.result })
                    setFormData((prevState) => ({ ...prevState, imagesList, multipleUpload: true }))
                };
            })();
            reader.readAsDataURL(f);
        }
    }

    //@Purpose : submitting step1 after step1 is validated
    const submitGeneralDetails = async (val) => {
        let { selectedStore, productName, description, brandName, selectedCategory, box, sku, barcode, imgs_Arr } = formData
        if (val ? true : step1validateForm()) {
            setLoading(true)
            let form_Data = new FormData();
            form_Data.append("category_id", selectedCategory);
            form_Data.append("store_id", selectedStore);
            form_Data.append("name", productName);
            description && form_Data.append("description", description);
            brandName && form_Data.append("brand_name", brandName);
            form_Data.append("item_kit", box);
            form_Data.append("sku", sku);
            // form_Data.append("app_token", APP_TOKEN);
            val && form_Data.append("action", 'save_and_exit');
            barcode && form_Data.append("barcode", barcode);

            if (Array.isArray(imgs_Arr) && imgs_Arr.length !== 0) {
                imgs_Arr.forEach(val => {
                    form_Data.append(`product_image[]`, val);
                });
            } else {
                form_Data.append('product_image[]', imgs_Arr);
            }
            let response = await commonApi(`products/create`, form_Data, 'post', authorized)
            if (response && response.status === 201) {
                setLoading(false)
                let { data } = response
                storeDetails(data.success.data.store)
                if (val && val === 'goback') {
                    props.history.push('/catelogue-list')
                } else
                    setFormData((prevState) => ({ ...prevState, step1: false, step2: true, productSlug: data.success.data.slug, step1Finished: true }))
            }
        }
    }

    //@Purpose : submitting step2 after step2 is validated
    const submitFurtherDetails = async (val) => {
        let { weight_measure, total_weight, typically_availablity, availableDays, volume_unit_cost, packaging_length, noOfpcs, packaging_width, packaging_height, standard_cost, unit_cost, volume_discount, availability, tags, productSlug } = formData
        if (val ? true : step2validateForm()) {
            setLoading(true)
            let request = {
                is_unit_cost_same: 'Yes',
                volume_unit_cost: volume_unit_cost,
                typically_availablity: typically_availablity ? (typically_availablity === 'In Stock' ? typically_availablity : availableDays + " " + typically_availablity) : typically_availablity,
                // availability,
                // no_of_pcs: noOfpcs ? noOfpcs : 0,
                tags
            }
            availability && (request['availability'] = availability)
            unit_cost && (request['unit_cost'] = unit_cost)
            standard_cost && (request['standard_cost'] = standard_cost)
            weight_measure && (request['weight_measure'] = weight_measure)
            total_weight && (request['total_weight'] = total_weight)
            packaging_length && (request['packaging_length'] = packaging_length)
            packaging_width && (request['packaging_width'] = packaging_width)
            packaging_height && (request['packaging_height'] = packaging_height)
            volume_discount && (request['volume_discount'] = volume_discount)
            noOfpcs && (request['no_of_pcs'] = noOfpcs)
            val && (request['action'] = 'save_and_exit')
            // ${productSlug}
            let response = await commonApi(`products/${productSlug}/further-details`, request, 'post', authorized)
            if (response && response.status === 200) {
                
                setLoading(false)
                let { data } = response
                storeDetails(data.success.data.store)
                showSuccessToast(data.success.message)
                props.history.push('/catelogue-list')
            }
        }
    }

    //@Purpose : updating product details after step1 and step2 are verified
    const updateProductDetails = async (value,novalid) => {
        let { productName, description, brandName, availableDays, selectedCategoryId, box, sku, barcode, imgs_Arr, typically_availablity, selectedStoreId,
            weight_measure, total_weight, volume_unit_cost, packaging_length, packaging_width, packaging_height, standard_cost, unit_cost, volume_discount, availability, tags } = formData
        if (novalid?true:(step1 === true ? step1validateForm() : step1validateForm() && step2validateForm())) {
            setLoading(true)
            let form_Data = new FormData();
            form_Data.append("_method", 'PATCH');
            form_Data.append("category_id", selectedCategoryId);
            form_Data.append("store_id", selectedStoreId);
            form_Data.append("name", productName);
            description && form_Data.append("description", description);
            brandName && form_Data.append("brand_name", brandName);
            form_Data.append("item_kit", box);
            form_Data.append("sku", sku);
            barcode && form_Data.append("barcode", barcode);
            form_Data.append("weight_measure", weight_measure ? weight_measure : 'Kg');
            form_Data.append("total_weight", total_weight ? total_weight : 0);
            form_Data.append("packaging_length", packaging_length ? packaging_length : 0);
            form_Data.append("packaging_width", packaging_width ? packaging_width : 0);
            form_Data.append("packaging_height", packaging_height ? packaging_height : 0);
            form_Data.append("standard_cost", standard_cost ? standard_cost : 0);
            form_Data.append("unit_cost", unit_cost ? unit_cost : 0);
            form_Data.append("volume_discount", volume_discount ? volume_discount : 0);
            form_Data.append("is_unit_cost_same", 'Yes');
            form_Data.append("volume_unit_cost", volume_unit_cost ? volume_unit_cost : 0);
            form_Data.append("no_of_pcs", noOfpcs ? noOfpcs : 0);
            value==='step1'&&form_Data.append("step", 1);
            typically_availablity && form_Data.append("typically_availablity", typically_availablity ? (typically_availablity === 'In Stock' ? typically_availablity : availableDays + " " + typically_availablity) : typically_availablity);
            form_Data.append("availability", availability !== "" ? availability : 'Available');

            // if(novalid){
                // form_Data.append['action'] = 'save_and_exit'
                novalid&& form_Data.append("action", 'save_and_exit');

            // }

            // form_Data.append("app_token", APP_TOKEN);

            if (Array.isArray(tags) && tags.length !== 0) {
                tags.forEach(val => {
                    form_Data.append(`tags[]`, val);
                });
            } else {
                form_Data.append('tags[]', tags);
            }

            if (Array.isArray(imgs_Arr) && imgs_Arr.length !== 0) {
                imgs_Arr.forEach(val => {
                    form_Data.append(`product_image[]`, val);
                });
            } else {
                form_Data.append('product_image[]', imgs_Arr);
            }
            let response = await commonApi(`products/${prod_slug}`, form_Data, 'post', authorized)
            if (response && response.status === 200) {
                setLoading(false)

                let { data } = response
                storeDetails(data.success.data.store)

                if (value === 'step1') {
                    setFormData({ ...formData, step1: false, step2: true })
                } else if (value && value === 'goback') {
                    props.history.push(`/catelogue-list?storeid=${selectedStoreId}`)
                }
                else {
                    showSuccessToast(data.success.message)
                    props.history.push(`/catelogue-list?storeid=${selectedStoreId}`)
                }
            }
        }
    }


   
    const fileUpload =  (imageUrl) => {
        console.log('fileUpload',imageUrl);

        let img_Arr=formData.imgs_Arr
        img_Arr.push(imageUrl)
        setFormData((prevState) => ({ ...prevState, imagesList:img_Arr, multipleUpload: true }))
        setisModelOpen(false);setImageUrl('')

        // setFormData((prevState) => ({ ...prevState, imagesList, multipleUpload: true }))
    };
    // console.log("imageUrl2", formData.imagesList);





    let { step1, step2, step1Finished, prevImages, prod_slug, selectedStore, storesList, multipleUpload, productName, volume_unit_cost, description, brandName, categoryList, selectedCategory, box, sku, barcode, imagesList, errors, typically_availablity,
        weight_measure, total_weight, availableDays, packaging_length, packaging_width, packaging_height, standard_cost, unit_cost, volume_discount, noOfpcs, availability, tags } = formData
    return (
        <React.Fragment>
            <div className="supplier-sec">
                <section className="head-sec d-flex justify-content-between align-items-center">
                    <h6 className="mb-0">Complete your profile</h6>
                    <h6 className="mb-0">{prod_slug === '' ? 'Add' : 'Edit'} Product</h6>
                    <button type="button" className="btn btn-primary" onClick={() => props.history.push('/catelogue-list')}>Cancel</button>
                </section>
                <section className="profile-frm">
                    <ul className="steps d-flex align-items-center add-product-step">
                        <li>
                            <div className={step1 || step2 ? "text-center active" : "text-center"}>
                                <span>1</span>
                                <h6>General Details</h6>
                            </div>
                            <div className="prog-bar"></div>
                        </li>
                        <li>
                            <div className={step2 ? "text-center active" : "text-center"}>
                                <span>2</span>
                                <h6>Further Details</h6>
                            </div>
                        </li>
                    </ul>
                    <div className="container">
                        {step1 ?
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="frm mt-0">
                                        <h6>Complete the form below to add individual products</h6>
                                        <form>
                                            <div className="row">
                                                <div className="form-group col-md-6">
                                                    <label>Supplier Store Name*</label>
                                                    {/* <select className="form-control mb-0" value={selectedStore} onFocus={() => getStoreList()} onChange={(e) => { errors["selectedStore"] = ""; setFormData({ ...formData, selectedStore: e.target.value, errors: errors }) }} >
                                                        <option>{selectedStoreId ? selectedStoreId : "Select"}</option>
                                                        {storesList.length !== 0 && storesList.map((store_, id) => {
                                                            return (
                                                                <option value={store_.id} key={id}>{store_.store_name}</option>
                                                            )
                                                        })}
                                                    </select> */}
                                                    <Select className="form-control mb-0"
                                                        // placeholder="Select a person"
                                                        showArrow={false}
                                                        notFoundContent={storesList.length === 0 ? <Spin size="small" /> : null}
                                                        value={selectedStore}
                                                        onFocus={() => getStoreList()}
                                                        onChange={(e) => { errors["selectedStore"] = ""; setFormData({ ...formData, selectedStore: e, selectedStoreId: e, errors: errors }) }} >
                                                        {/* <Select.Option>{selectedStoreId ? selectedStoreId : "Select"}</Select.Option> */}
                                                        {storesList.length !== 0 && storesList.map((store_, id) => {
                                                            return (
                                                                <Select.Option value={store_.id} key={id}>{store_.store_name}</Select.Option>
                                                            )
                                                        })}
                                                    </Select>
                                                    <p className="error-msg"> {errors.selectedStore}</p>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Product Name*</label>
                                                    <input type="text" className="form-control mb-0" name="productName" placeholder='Ex. Tinned Tomatoes' value={productName} onChange={(e) => handleChange(e)} />
                                                    <p className="error-msg"> {errors.productName}</p>
                                                </div>
                                                <div className="form-group col-md-12">
                                                    <label>Product Description (Max. 300 characters)</label>
                                                    <textarea className="form-control mb-0" name="description" value={description} placeholder='Enter product detail here' onChange={(e) => handleChange(e)}></textarea>
                                                    <p className="error-msg"> {errors.description}</p>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Product Brand Name</label>
                                                    <input type="text" className="form-control mb-0" name="brandName" value={brandName} placeholder='Brand Name' onChange={(e) => handleChange(e)} />
                                                    <p className="error-msg"> {errors.brandName}</p>

                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Product Category*</label>
                                                    {/* <select className="form-control mb-0" value={selectedCategory} onFocus={() => CategoriesList()} onChange={(e) => { errors["selectedCategory"] = ""; setFormData({ ...formData, selectedCategory: e.target.value, errors: errors }) }} >
                                                        <option>{selectedCategoryId ? selectedCategoryId : 'Select'}</option>
                                                        {categoryList.length !== 0 && categoryList.map((category_, id) => {
                                                            return (
                                                                <option value={category_.id} key={id}>{category_.name}</option>
                                                            )
                                                        })}
                                                    </select> */}
                                                    <Select className="form-control mb-0"
                                                        value={selectedCategory}
                                                        onFocus={() => CategoriesList()}
                                                        notFoundContent={categoryList.length === 0 ? <Spin size="small" /> : null}
                                                        onChange={(e) => { errors["selectedCategory"] = ""; setFormData({ ...formData, selectedCategory: e, selectedCategoryId: e, errors: errors }) }}>
                                                        {/* <Select.Option>{selectedCategoryId ? selectedCategoryId : 'Select'}</Select.Option> */}
                                                        {categoryList.length !== 0 && categoryList.map((category_, id) => {
                                                            return (
                                                                <Select.Option value={category_.id} key={id}>{category_.name}</Select.Option>
                                                            )
                                                        })}
                                                    </Select>
                                                    <p className="error-msg"> {errors.selectedCategory}</p>
                                                </div>
                                                <div className="col-md-12 form-group radio-btn">
                                                    <div className="form-check-inline">
                                                        <label className="form-check-label">
                                                            <input type="radio" className="form-check-input" name="optradio" checked={box === 'Item' ? true : false} onChange={() => { errors["box"] = ""; setFormData({ ...formData, box: 'Item', errors: errors }) }} />Item
										                  <span></span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check-inline">
                                                        <label className="form-check-label">
                                                            <input type="radio" className="form-check-input" name="optradio" checked={box === 'Kit' ? true : false} onChange={() => { errors["box"] = ""; setFormData({ ...formData, box: 'Kit', errors: errors }) }} />Kit
										                  <span></span>
                                                        </label>
                                                    </div>
                                                    <p className="error-msg"> {errors.box}</p>

                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>SKU*</label>
                                                    <div className="info-tooltip">
                                                        <img src="/assests/images/info.svg" alt="" />
                                                        <div className="arrow_box"> Enter the unique code that can be used to track inventory and report and sales. Ensure it is simple to understand and can help with picking and packing orders. </div>
                                                    </div>
                                                    <input type="text" className="form-control mb-0" placeholder='SKU0101' name="sku" value={sku} onChange={(e) => handleChange(e)} />
                                                    <p className="error-msg"> {errors.sku}</p>

                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Barcode (UPC / EAN)</label>
                                                    <input type="text" className="form-control mb-0" name="barcode" placeholder='010101' value={barcode} onChange={(e) => handleChange(e)} />
                                                    <p className="error-msg"> {errors.barcode}</p>
                                                </div>
                                                <div className="col-md-12 form-group">
                                                    <label>Add Images</label>
                                                    <div className="file-upload">
                                                        <span>(You can upload JPEG or PNG with maximum size of 5MB)</span>
                                                        <div className="d-flex">
                                                            <div className="custom-file mb-0">
                                                                {/* <input
                                                                    type="file"
                                                                    className="hide-file"
                                                                    name='image'
                                                                    id='file1'
                                                                    accept='images/*'
                                                                    multiple
                                                                    onChange={(e) => fileChangedHandler(e)}
                                                                /> */}
                                                                <ImageCrop fileUpload={fileUpload}/>
                                                                <label className="custom-file-label" for="file">
                                                                    <img src="/assests/images/add.svg" alt='iconimage' /></label>
                                                            </div>
                                                            {multipleUpload ?
                                                                <React.Fragment>
                                                                    <div className="customs-file-img">
                                                                        {imagesList && Array.isArray(imagesList) && imagesList.length ? imagesList.map((each, id) => {
                                                                            return (
                                                                                <React.Fragment key={id}>
                                                                                    <React.Fragment >
                                                                                        <img src={each && each ? each : undefined} alt='iconimage' width="100px" />
                                                                                    </React.Fragment>
                                                                                </React.Fragment>
                                                                            )
                                                                        }) : undefined}
                                                                    </div>
                                                                </React.Fragment>
                                                                : null}
                                                            <div className="customs-file-img">
                                                                {prevImages.length !== 0 && prevImages.map((image) => {

                                                                    return (
                                                                        <img src={image.image_thumb_url} alt='product_image' />
                                                                    )

                                                                })}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-md-12 text-right">
                                                    {prod_slug === '' ?
                                                        <button type="button" className="btn btn-primary" onClick={() => step1Finished === true ? props.history.push('/catelogue-list') : submitGeneralDetails('goback')}>Save & Exit</button> :
                                                        <button type="button" className="btn btn-primary" onClick={() => updateProductDetails('goback','novalid')}>Save & Exit</button>
                                                    }
                                                    {prod_slug === '' ?
                                                        <button type="button" className="btn btn-primary" onClick={() => step1Finished === true ? setFormData({ ...formData, step1: false, step2: true }) : submitGeneralDetails()}>Save & Next</button> :
                                                        <button type="button" className="btn btn-primary" onClick={() => updateProductDetails('step1')}>Save & Next</button>}

                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            : null}
                        {step2 ?
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="frm single-frm-2">
                                        <h6>Enter further details of the product here</h6>
                                        <form>
                                            <div className="row">
                                                <div className="form-group col-md-3">
                                                    <label>Weight Measure</label>
                                                    <div className="info-tooltip">
                                                        <img src="/assests/images/info.svg" alt="" />
                                                        <div className="arrow_box">Select the measure of weight for this item / kit.</div>
                                                    </div>
                                                    <select className="form-control mb-0" value={weight_measure} onChange={(e) => { errors["weight_measure"] = ""; setFormData({ ...formData, weight_measure: e.target.value, errors: errors }) }}>
                                                        <option >Select</option>
                                                        <option value="Kg">Kg</option>
                                                        <option value="Gr">Gr</option>
                                                        <option value="Litre">Litre</option>
                                                    </select>
                                                    {/* <p className="error-msg"> {errors.weight_measure}</p> */}
                                                </div>
                                                <div className="form-group col-md-3">
                                                    <label>Total Weight</label>
                                                    <div className="info-tooltip">
                                                        <img src="/assests/images/info.svg" alt="" />
                                                        <div className="arrow_box">Enter the total weight of the product being sold.</div>
                                                    </div>
                                                    <input type="number" className="form-control mb-0" name='total_weight' placeholder='10' value={total_weight} onChange={(e) => handleChange(e)} />
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Number of pcs</label>
                                                    <input type="number" className="form-control mb-0" name='noOfpcs' value={noOfpcs} placeholder="10" onChange={(e) => handleChange(e)} />
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Packaging Length (mm)</label>
                                                    <input type="number" className="form-control mb-0" name='packaging_length' value={packaging_length} placeholder="LL" onChange={(e) => handleChange(e)} />
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Packaging Width (mm)</label>
                                                    <input type="number" className="form-control mb-0" name='packaging_width' value={packaging_width} placeholder="WW" onChange={(e) => handleChange(e)} />
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Packaging Height (mm)</label>
                                                    <input type="number" className="form-control mb-0" name='packaging_height' value={packaging_height} placeholder="DD" onChange={(e) => handleChange(e)} />
                                                </div>
                                                {/* <div className="form-group col-md-6">
                                                    <label>Typical Availability</label>
                                                    <select className="form-control col-md-4" value={weight_measure} onChange={(e) => { errors["weight_measure"] = ""; setFormData({ ...formData, weight_measure: e.target.value, errors: errors }) }}>
                                                        <option >Select</option>
                                                        <option value="In Stock">In Stock</option>
                                                        <option value="days">XX days</option>
                                                        <option value="weeks">XX weeks</option>
                                                    </select>
                                                    <input type="text" className="form-control mb-0" value={typically_availablity} name="typically_availablity" onChange={(e) => handleChange(e)} placeholder="In stock / XX Days / XX weeks" />
                                                </div> */}
                                                <div className="form-group col-md-3">
                                                    <label>Typical Availability</label>
                                                    <select className="form-control col-md-0" value={typically_availablity} name='typically_availablity' onChange={(e) => handleChange(e)}>
                                                        <option >Select</option>
                                                        <option value="In Stock">In Stock</option>
                                                        <option value="days">In XX days</option>
                                                        <option value="weeks">In XX weeks</option>
                                                    </select>
                                                </div>
                                                {typically_availablity !== 'In Stock' && typically_availablity ? <div className="form-group col-md-3">
                                                    <label>No of {typically_availablity}</label>
                                                    <div className="info-tooltip">
                                                        <img src="images/info.svg" alt="" />
                                                        <div className="arrow_box">Test</div>
                                                    </div>
                                                    <input type="number" className="form-control mb-0" name='availableDays' placeholder='10' value={availableDays} onChange={(e) => handleChange(e)} />
                                                    <p className="error-msg"> {errors.availableDays}</p>
                                                </div> : null}

                                                <div className="form-group col-md-6">
                                                    <label>Unit Cost (ex GST)*</label>
                                                    <input type="number" className="form-control mb-0" placeholder="XX.XX" name='unit_cost' value={unit_cost} onChange={(e) => handleChange(e)} />
                                                    <p className="error-msg"> {errors.unit_cost}</p>
                                                    <span className="d-sign">$</span>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Standard Costing MOQs (pcs)*</label>
                                                    <div className="info-tooltip">
                                                        <img src="/assests/images/info.svg" alt="" />
                                                        <div className="arrow_box">Enter the minimum quantity that will be sold of this product.</div>
                                                    </div>
                                                    <input type="number" className="form-control mb-0" name='standard_cost' placeholder='10' value={standard_cost} onChange={(e) => handleChange(e)} />
                                                    <p className="error-msg"> {errors.standard_cost}</p>
                                                </div>

                                                {/* <div className="col-md-12 form-group">
                                                    <div className="form-check mb-0">
                                                        <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                                        <label className="form-check-label" for="exampleCheck1">Unit Cost (Ex. GST) same for every store?</label>
                                                    </div>
                                                </div>*/}
                                                <div className="form-group col-md-6">
                                                    <label>Volume Unit Cost (ex GST)</label>
                                                    <input type="text" className="form-control mb-0" placeholder="XX.XX" value={volume_unit_cost} name="volume_unit_cost" onChange={(e) => handleChange(e)} />
                                                    <span className="d-sign">$</span>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Volume Discount MOQs (pcs)</label>
                                                    <div className="info-tooltip">
                                                        <img src="/assests/images/info.svg" alt="" />
                                                        <div className="arrow_box">Enter the volume quantity that will be sold of this product at the volume pricing</div>
                                                    </div>
                                                    <input type="number" className="form-control mb-0" name='volume_discount' placeholder="10" value={volume_discount} onChange={(e) => handleChange(e)} />
                                                </div>

                                                <div className="form-group col-md-12">
                                                    <label>Product tags for search functionality*</label>
                                                    <div className="info-tooltip">
                                                        <img src="/assests/images/info.svg" alt="" />
                                                        <div className="arrow_box">Add tags to the product so that the buyer can find your product easily when searching. Include things like product name, category, brand, etc..</div>
                                                    </div>
                                                    {/* <input type="text" className="form-control" placeholder="Enter tags separated by commas" /> */}
                                                    <Select
                                                        mode="tags"
                                                        style={{ width: '100%' }}
                                                        value={tags}
                                                        placeholder="Please enter tags "
                                                        onChange={(value) => { errors["tags"] = ""; setFormData({ ...formData, tags: value, errors: errors }) }}>
                                                    </Select>
                                                    <p className="error-msg"> {errors.tags}</p>
                                                </div>

                                                <div className="col-md-12 form-group radio-btn">
                                                    <div className="form-check-inline">
                                                        <label className="form-check-label">
                                                            <input type="radio" className="form-check-input" name="optradio" checked={availability === 'Available' ? true : false} onChange={() => { errors["availability"] = ""; setFormData({ ...formData, availability: 'Available', errors: errors }) }} />Available
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check-inline">
                                                        <label className="form-check-label">
                                                            <input type="radio" className="form-check-input" name="optradio" checked={availability === 'Coming Soon' ? true : false} onChange={() => { errors["availability"] = ""; setFormData({ ...formData, availability: 'Coming Soon', errors: errors }) }} />Coming Soon
                                                             <span></span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check-inline">
                                                        <label className="form-check-label">
                                                            <input type="radio" className="form-check-input" name="optradio" checked={availability === 'Low On Stock' ? true : false} onChange={() => { errors["availability"] = ""; setFormData({ ...formData, availability: 'Low On Stock', errors: errors }) }} />Low on stock
                                                             <span></span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check-inline">
                                                        <label className="form-check-label">
                                                            <input type="radio" className="form-check-input" name="optradio" checked={availability === 'Out of Stock' ? true : false} onChange={() => { errors["availability"] = ""; setFormData({ ...formData, availability: 'Out of Stock', errors: errors }) }} />Out of Stock
                                                             <span></span>
                                                        </label>
                                                    </div>
                                                    <p className="error-msg"> {errors.availability}</p>
                                                </div>
                                                <div className="col-md-12 text-right d-flex justify-content-between">
                                                    <button type="button" className="btn btn-primary" onClick={() => setFormData({ ...formData, step1: true, step2: false })}>Previous</button>
                                                    <div>
                                                        {prod_slug === '' ?
                                                            <button type="button" className="btn btn-primary" onClick={() => submitFurtherDetails('goback')}>Save &amp; Exit</button> :
                                                            <button type="button" className="btn btn-primary" onClick={() => updateProductDetails('goback','novalid')}>Save &amp; Exit</button>}

                                                        {prod_slug === '' ?
                                                            <button type="button" className="btn btn-primary" onClick={() => submitFurtherDetails()}>Add Product</button> :
                                                            <button type="button" className="btn btn-primary" onClick={() => updateProductDetails()}>Update Product</button>}

                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            : null}
                    </div>
                </section>
            </div>
        </React.Fragment>
    );
};

export default connect(null, { commonApi,storeDetails })(AddSingleProduct)


